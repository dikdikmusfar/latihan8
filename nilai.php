<?php
function tentukan_nilai($number)
{
    if ($number<=100 && $number>=85) {
        echo "<br> Nilai = $number Sangat Baik";
    } elseif ($number<85 && $number>=70) {
        echo "<br> Nilai = $number Baik";
    } elseif ($number<70 && $number>=60){
        echo "<br> Nilai = $number Cukup";
    } else {
        echo "<br>  Nilai = $number Kurang";
    }
    
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>